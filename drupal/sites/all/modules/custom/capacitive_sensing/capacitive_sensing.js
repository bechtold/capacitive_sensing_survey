(function ($) {
  Drupal.behaviors.capacitiveSensing = {
    attach: function (context, settings) {

      //save and next button functionality
      $('.view-papers .form-submit.save-next', context).click(function () {
        var row = $(this).parents('.megarow');
        setTimeout(function() {
          row.next().find('.views-megarow-open').click();
          row.find('.close').click();
        }, 1000);
      });

      //close on save
      $('.view-papers .form-submit.save', context).click(function () {
        var row = $(this).parents('.megarow');
        setTimeout(function() {
          row.find('.close').click();
        }, 1000);
      });

      // set status and assigned to values from next fieldset
      var form = $('.megarow .node-paper-form');
      if(form.length > 0){
        var assignee = $('#edit-nextassignee').val();
        var status = $('#edit-nextstatus').val();
        if(assignee !== "All") {
          field = form.find('.field-name-field-assigned');
          field.find('select').val(assignee);
          field.find('.suffix').addClass('status');
        }
        if(status !== "All") {
          field = form.find('.field-name-field-status');
          field.find('select').val(status);
          field.find('.suffix').addClass('status');
        }
      }

      // editable fields auto submit on hot or not page
      $('.editablefield-item').once('editablefield', function() {
        var $this = $(this);

        // There is only one editable field in that form, we can hide the submit
        // button.
        if ($this.find('.field-name-field-trash').length == 1) {
          $this.find('input.form-submit').hide();
          $this.find('input[type=radio]').change(function() {
            $this.find('input.form-submit').triggerHandler('click');
          });
        }
      });


        //$("select").attr("size",$(" option").length);
        $(".node-paper-form select").each(function(id, el){
          $(el).attr("size", $(el).find('option').length);
        });


    }
  };
})(jQuery);
