<?php


function capacitive_sensing_bibtex_action(&$node, $context) {
  $paper = entity_metadata_wrapper('node', $node);
//  dpm($paper->field_doi->value());
  $doi = $paper->field_doi->value();
  if(!empty($doi)) {
    // fetch data
    $bibtex = drupal_http_request('http://api.crossref.org/works/' . $paper->field_doi->value() . '/transform/application/x-bibtex');


    if($bibtex->code == 200) {
      $paper->field_bibtex->set($bibtex->data);
    }
  }
}

function capacitive_sensing_optimize_bibtex_ids_action(&$node, $context) {
//  dpm($context, 'context');
  $paper = entity_metadata_wrapper('node', $node);
  $context['papers'][] = $paper->title->value();

  ctools_include('object-cache');
  $cached = ctools_object_cache_get('capacitive_sensing', 'ids');

  $bibtex = $paper->field_bibtex->value();
  $from = strpos($bibtex, '{');
  $to = strpos($bibtex, ',');
  $id = substr($bibtex, $from + 1, $to - $from - 1);

  if(in_array($id, $cached)){
    $bibtex = str_replace($id, $id."_1", $bibtex);
    $paper->field_bibtex->set($bibtex);
    $cached[] = $id."_1";
    $cached['counter']++;
  } else {
    $cached[] = $id;
  }

  ctools_object_cache_set("capacitive_sensing", "ids", $cached);

  if($context['progress']['current'] == $context['progress']['total']){
    drupal_set_message('Optimized '. ($cached['counter'] ? $cached['counter'] : "0") . ' IDs');
    ctools_object_cache_clear("capacitive_sensing", "ids");
  }
}


function capacitive_sensing_add_title_to_bibtex_ids_action(&$node, $context) {
//  dpm($context, 'context');
  $paper = entity_metadata_wrapper('node', $node);
  $context['papers'][] = $paper->title->value();


  $bibtex = $paper->field_bibtex->value();
  $from = strpos($bibtex, '{');
  $to = strpos($bibtex, ',');
  $id = substr($bibtex, $from + 1, $to - $from - 1);

  $title = $paper->title->value();
  $cleaned_title = str_replace(' ', '_', $title);
  $cleaned_title = preg_replace('/[^A-Za-z0-9\_]/', '', $cleaned_title);
  $cleaned_title = explode('_',trim($cleaned_title));

  $bibtex = str_replace($id, $id."_".$cleaned_title[0]."_".$cleaned_title[1], $bibtex);
  $paper->field_bibtex->set($bibtex);


//  if($context['progress']['current'] == $context['progress']['total']){
//  }
}

function capacitive_sensing_export_bibtex_action(&$node, $context) {
  $paper = entity_metadata_wrapper('node', $node);
//  dpm($paper);
//  dpm($context, 'context');
  $filename = 'references.bib'; // xml,pdf,doc etc as you needed
  $filepath = 'public://' .$filename;
  if($context['progress']['current'] == 1) {
    $file = file_save_data("", $filepath, FILE_EXISTS_REPLACE);
  }

  $realpath = drupal_realpath($filepath);

  $content = $paper->field_bibtex->value();

  file_put_contents($realpath, $content."\r\n", FILE_APPEND | LOCK_EX);

  if($context['progress']['current'] == $context['progress']['total']) {
    drupal_set_message( l('Download references.bib', file_create_url($filepath), array('attributes' => array('target'=>'_blank')) ));
  }
}

function capacitive_sensing_export_json_action(&$node, $context) {
  $paper = entity_metadata_wrapper('node', $node);
//  dpm($paper);
//  dpm($context, 'context');
  $filename = 'export.json'; // xml,pdf,doc etc as you needed
  $filepath = 'public://' .$filename;
  if($context['progress']['current'] == 1) {
    $file = file_save_data("[\r\n", $filepath, FILE_EXISTS_REPLACE);
  }

  $realpath = drupal_realpath($filepath);

  $content = "{";
  $content .= '"title":"'.$paper->title->value()."\",\r\n";
  $content .= '"doi":"'.$paper->field_doi->value()."\",\r\n";
  $content .= '"authors":'._capacative_sensing_terms_to_json($paper->field_authors->value()).",\r\n";
  $content .= '"date":"'.$paper->field_date->value()."\",\r\n";
  $content .= '"search_date":"'.$paper->field_search_date->value()."\",\r\n";
  $content .= '"search_query":'._capacative_sensing_terms_to_json($paper->field_search_query->value()).",\r\n";
  $content .= '"keywords":'._capacative_sensing_terms_to_json($paper->field_keywords->value()).",\r\n";
  $content .= '"publisher":'._capacative_sensing_terms_to_json($paper->field_publisher->value()).",\r\n";
  $content .= '"conference":'._capacative_sensing_terms_to_json($paper->field_conference->value()).",\r\n";

  $content .= '"measurement_principle":'._capacative_sensing_terms_to_json($paper->field_measurement_principle->value()).",\r\n";
  $content .= '"sensing_system":'._capacative_sensing_terms_to_json($paper->field_sensing_system->value()).",\r\n";
  $content .= '"sensing_mode":'._capacative_sensing_terms_to_json($paper->field_sensing_mode->value()).",\r\n";
  $content .= '"electrode_configuration":'._capacative_sensing_terms_to_json($paper->field_electrode_configuration->value()).",\r\n";
  $content .= '"sensing_goal":'._capacative_sensing_terms_to_json($paper->field_sensing_goal->value()).",\r\n";
  $content .= '"body_parts_affected":'._capacative_sensing_terms_to_json($paper->field_body_parts_affected->value()).",\r\n";
  $content .= '"fabrication":'._capacative_sensing_terms_to_json($paper->field_fabrication->value()).",\r\n";
  $content .= '"material":'._capacative_sensing_terms_to_json($paper->field_material->value()).",\r\n";
  $content .= '"substrate":'._capacative_sensing_terms_to_json($paper->field_substrate->value()).",\r\n";
  $content .= '"machine_learning":'._capacative_sensing_terms_to_json($paper->field_machine_learning->value()).",\r\n";
  $content .= '"contributions":'._capacative_sensing_terms_to_json($paper->field_contributions->value()).",\r\n";
  $content .= '"accompanying_technologies":'._capacative_sensing_terms_to_json($paper->field_accompanying_technologies->value()).",\r\n";
  $content .= '"interaction_space_area":'._capacative_sensing_terms_to_json($paper->field_interaction_space_area->value()).",\r\n";
  $content .= '"max_sensing_range_cm_":"'.$paper->field_max_sensing_range_cm_->value()."\",\r\n";
  $content .= '"number_of_sensors":"'.$paper->field_number_of_sensors->value()."\",\r\n";
  $content .= '"spatial_resolution_mm_":"'.$paper->field_spatial_resolution_mm_->value()."\",\r\n";
  $content .= '"update_rate_hz_":"'.$paper->field_update_rate_hz_->value()."\",\r\n";
  $content .= '"latency_ms_":"'.$paper->field_latency_ms_->value()."\",\r\n";
  $content .= '"object_form_factor_":'._capacative_sensing_terms_to_json($paper->field_object_form_factor_->value())."\r\n";
//  $content .= '"problem_field":"'.addslashes($paper->field_problem_field->value())."\",\r\n";
//  $content .= '"comment":"'.addslashes($paper->field_comment->value())."\"\r\n";

  $content .= "}";
  if($context['progress']['current'] != $context['progress']['total']) {
    $content .= ",";
  }

  file_put_contents($realpath, $content."\r\n", FILE_APPEND | LOCK_EX);

  if($context['progress']['current'] == $context['progress']['total']) {
    file_put_contents($realpath, "]\r\n", FILE_APPEND | LOCK_EX);
    drupal_set_message( l('Download export.json', file_create_url($filepath), array('attributes' => array('target'=>'_blank')) ));
  }
}

function _capacative_sensing_terms_to_json($terms){
  if(is_array($terms)) {
    $output = "[";
    $i = 0;
    $len = count($terms);
    foreach ($terms as $term) {
      $output .= '"' . $term->name . '"';
      if ($i != $len - 1) {
        $output .= ',';
      }
      $i++;
    }
    $output .= "]";
  } else {
    $output = "[\"".$terms->name."\"]";
  }
  return $output;
}