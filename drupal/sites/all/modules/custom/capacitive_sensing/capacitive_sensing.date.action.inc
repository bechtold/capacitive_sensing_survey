<?php

function capacitive_sensing_date_action(&$node, $context) {

  $paper = entity_metadata_wrapper('node', $node);
  $doi = $paper->field_doi->value();
  if(!empty($doi)) {
    // fetch data
    $bibtex = drupal_http_request('http://api.crossref.org/works/' . $paper->field_doi->value() . '');


    if($bibtex->code == 200) {
      $json = json_decode($bibtex->data);
      if($json->status == 'ok'){
//        dpm($json, 'json');
        $nodedate = new DateTime();
        $nodedate->setTimestamp($paper->field_date->value());

        $rdate = get_object_vars($json->message->issued);
        $restdate = $rdate['date-parts'][0];
        //if years are different use the one from REST
        if($nodedate->format('Y') != $restdate[0]){

//          dpm($node->title, 'date needs update');

          $newdate = new DateTime();
          $newdate->setDate($restdate[0], isset($restdate[1]) ? $restdate[1] : 1,  isset($restdate[2]) ? $restdate[2] : 1);
          $paper->field_date->set($newdate->getTimestamp());

        }
      }
//      $paper->field_bibtex->set($bibtex->data);
    }
  }

}